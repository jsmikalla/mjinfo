import { expect } from 'chai';
import 'mocha';

import Tile from "@/lib/mahjong/model/Tile";
import Hand from "@/lib/mahjong/model/Hand";

import Quad from "@/lib/mahjong/model/suites/Quad";
import Triplet from "@/lib/mahjong/model/suites/Triplet";

import StandardSuiteSetDetection from "@/lib/mahjong/service/scoreCalculation/suiteSetDetection/StandardSuiteSetDetection";

describe('NormalHandsDetector', () => {

    it ('Should detect valid normal hands', () => {
        let result = StandardSuiteSetDetection.standardSuiteSetsOfHand(
            new Hand(
                [
                    new Tile('man', '2', false),
                    new Tile('man', '3', false),
                    new Tile('man', '4', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '4', false),
                    new Tile('pin', '5', true),
                    new Tile('pin', '6', false),
                    new Tile('pin', '4', false),
                ],
                [],
                [],
                [],
                [],
                new Tile('pin', '4', false)
            )
        );
        expect(result.length).to.equal(3);

        let result2 = StandardSuiteSetDetection.standardSuiteSetsOfHand(
            new Hand(
                [
                    new Tile('man', '2', false),
                    new Tile('man', '3', false),
                    new Tile('man', '4', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '4', false),
                    new Tile('pin', '5', true),
                    new Tile('pin', '6', false),
                    new Tile('pin', '4', false),
                ],
                [
                    new Quad(
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        false
                    )
                ],
                [],
                [],
                [],
                new Tile('pin', '4', false)
            )
        );
        expect(result2.length).to.equal(3);

        let result3 = StandardSuiteSetDetection.standardSuiteSetsOfHand(
            new Hand(
                [
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '4', false),
                    new Tile('pin', '5', true),
                    new Tile('pin', '6', false),
                    new Tile('pin', '4', false),
                ],
                [],
                [
                    new Triplet(
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        true
                    ),
                    new Triplet(
                        new Tile('bamboo', '4', false),
                        new Tile('bamboo', '4', false),
                        new Tile('bamboo', '4', false),
                        true
                    ),
                ],
                [],
                [],
                new Tile('pin', '4', false)
            )
        );
        expect(result3.length).to.equal(3);

        let result4 = StandardSuiteSetDetection.standardSuiteSetsOfHand(
            new Hand(
                [
                    new Tile('pin', '4', false),
                ],
                [
                    new Quad(
                        new Tile('wind', 'east', false),
                        new Tile('wind','east', false),
                        new Tile('wind', 'east', false),
                        new Tile('wind', 'east', false),
                        false
                    )
                ],
                [
                    new Triplet(
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        new Tile('dragon', 'green', false),
                        true
                    ),
                    new Triplet(
                        new Tile('bamboo', '4', false),
                        new Tile('bamboo', '4', false),
                        new Tile('bamboo', '4', false),
                        true
                    ),
                ],
                [
                    new Quad (
                        new Tile('wind', 'east', false),
                        new Tile('wind','east', false),
                        new Tile('wind', 'east', false),
                        new Tile('wind', 'east', false),
                        false
                    )
                ],
                [],
                new Tile('pin', '4', false)
            )
        );
        expect(result4.length).to.equal(1);

        let result5 = StandardSuiteSetDetection.standardSuiteSetsOfHand(
            new Hand(
                [
                    new Tile('man', '2', false),
                    new Tile('man', '3', false),
                    new Tile('man', '4', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '4', false),
                    new Tile('pin', '5', true),
                    new Tile('pin', '6', false),
                    new Tile('pin', '4', false),
                ],
                [],
                [],
                [],
                [],
                new Tile('bamboo', '5', false)
            )
        );
        expect(result5.length).to.equal(0);

        let result6 = StandardSuiteSetDetection.standardSuiteSetsOfHand(
            new Hand(
                [
                    new Tile('man', '4', false),
                    new Tile('man', '3', false),
                    new Tile('man', '4', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('dragon', 'green', false),
                    new Tile('wind', 'east', false),
                    new Tile('wind','east', false),
                    new Tile('wind', 'east', false),
                    new Tile('pin', '4', false),
                    new Tile('pin', '5', true),
                    new Tile('pin', '6', false),
                    new Tile('pin', '4', false),
                ],
                [],
                [],
                [],
                [],
                new Tile('pin', '2', false)
            )
        );
        expect(result6.length).to.equal(0);
    });
});