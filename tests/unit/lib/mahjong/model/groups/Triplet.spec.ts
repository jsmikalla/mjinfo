import { expect } from 'chai';
import 'mocha';
import Tile, {TColor, TNumber} from "@/lib/mahjong/model/Tile";
import Triplet from "@/lib/mahjong/model/suites/Triplet";

describe('Pon', () => {

    let numberSimples: Array<TNumber> = ['2', '3', '4', '5', '6', '7', '8'];
    let numberTerminals: Array<TNumber> = ['1', '9'];
    let numberDragons: Array<TNumber> = ['red', 'white', 'green'];
    let numberWinds: Array<TNumber> = ['east', 'south', 'west', 'north'];
    let numberHonors: Array<TNumber> = numberWinds.concat(numberDragons);
    let numbers: Array<TNumber> = numberSimples.concat(numberTerminals).concat(numberDragons).concat(numberWinds);

    let colors = ['pin', 'man', 'bamboo', 'wind', 'dragon'];

    it('should not be constructed with duplicate tiles', () => {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile = new Tile(<TColor>color, number, false);
                    expect(() => {
                        new Triplet(tile, tile, tile, false);
                    }).to.throw();
                    expect(() => {
                        new Triplet(tile, tile, tile, true);
                    }).to.throw();
                }
            }
        }
    });

    it('should only be constructed with identical tiles', () => {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile1 = new Tile(<TColor>color, number, false);
                    let tile2 = new Tile(<TColor>color, number, false);
                    let tile3 = new Tile(<TColor>color, number, false);
                    expect(() => {
                        let newPon = new Triplet(tile1, tile2, tile3, false);

                        expect(newPon.firstTile === tile1).to.equal(true);
                        expect(newPon.secondTile === tile2).to.equal(true);
                        expect(newPon.thirdTile === tile3).to.equal(true);
                        expect(newPon.isOpen).to.equal(false);
                    }).to.not.throw();
                    expect(() => {
                        let newPon = new Triplet(tile1, tile2, tile3, true);

                        expect(newPon.firstTile === tile1).to.equal(true);
                        expect(newPon.secondTile === tile2).to.equal(true);
                        expect(newPon.thirdTile === tile3).to.equal(true);
                        expect(newPon.isOpen).to.equal(true);
                    }).to.not.throw();

                    if (number === '5') {
                        let tile5 = new Tile(<TColor>color, number, true);
                        let tile6 = new Tile(<TColor>color, number, false);
                        let tile7 = new Tile(<TColor>color, number, false);
                        expect(() => {
                            let newPon = new Triplet(tile5, tile6, tile7, false);

                            expect(newPon.firstTile === tile5).to.equal(true);
                            expect(newPon.secondTile === tile6).to.equal(true);
                            expect(newPon.thirdTile === tile7).to.equal(true);
                            expect(newPon.isOpen).to.equal(false);
                        }).to.not.throw();

                        expect(() => {
                            let newPon = new Triplet(tile5, tile6, tile7, true);

                            expect(newPon.firstTile === tile5).to.equal(true);
                            expect(newPon.secondTile === tile6).to.equal(true);
                            expect(newPon.thirdTile === tile7).to.equal(true);
                            expect(newPon.isOpen).to.equal(true);
                        }).to.not.throw();
                    }
                }
            }
        }

        expect(() => {
            new Triplet(
                new Tile('man', '5', true),
                new Tile('man', '5', false),
                new Tile('man', '6', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Triplet(
                new Tile('man', '5', false),
                new Tile('man', '6', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Triplet(
                new Tile('man', '6', false),
                new Tile('man', '5', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();

        expect(() => {
            new Triplet(
                new Tile('man', '5', false),
                new Tile('pin', '5', false),
                new Tile('man', '5', false),
                false
            );
        }).to.throw();
    });
});