import { expect } from 'chai';
import 'mocha';
import Tile, {TColor, TNumber} from "@/lib/mahjong/model/Tile";
import Pair from "@/lib/mahjong/model/suites/Pair";

describe('Pair', () => {

    let numberSimples: Array<TNumber> = ['2', '3', '4', '5', '6', '7', '8'];
    let numberTerminals: Array<TNumber> = ['1', '9'];
    let numberDragons: Array<TNumber> = ['red', 'white', 'green'];
    let numberWinds: Array<TNumber> = ['east', 'south', 'west', 'north'];
    let numberHonors: Array<TNumber> = numberWinds.concat(numberDragons);
    let numbers: Array<TNumber> = numberSimples.concat(numberTerminals).concat(numberDragons).concat(numberWinds);

    let colors = ['pin', 'man', 'bamboo', 'wind', 'dragon'];

    it('should not be constructed with duplicate tiles', () => {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile = new Tile(<TColor>color, number, false);
                    expect(() => {
                        new Pair(tile, tile);
                    }).to.throw();
                }
            }
        }
    });

    it('should only be constructed with identical tiles', () => {
        for (let color of colors) {
            for (let number of numbers) {
                if (Tile.isValidCombinationForTile(<TColor>color, number, false)) {
                    let tile1 = new Tile(<TColor>color, number, false);
                    let tile2 = new Tile(<TColor>color, number, false);
                    expect(() => {
                        let newPair = new Pair(tile1, tile2);

                        expect(newPair.firstTile === tile1).to.equal(true);
                        expect(newPair.secondTile === tile2).to.equal(true);
                    }).to.not.throw();

                    if (number === '5') {
                        let tile3 = new Tile(<TColor>color, number, true);
                        let tile4 = new Tile(<TColor>color, number, false);
                        expect(() => {
                            let newPair = new Pair(tile1, tile2);

                            expect(newPair.firstTile === tile1).to.equal(true);
                            expect(newPair.secondTile === tile2).to.equal(true);
                        }).to.not.throw();
                    }
                }
            }
        }

        for (let color1 of colors) {
            for (let number1 of numbers) {
                for (let color2 of colors) {
                    for (let number2 of numbers) {
                        if (
                            !(color1 === color2 && number1 === number2) &&
                            Tile.isValidCombinationForTile(<TColor>color1, number1, false) &&
                            Tile.isValidCombinationForTile(<TColor>color2, number2, false)
                        ) {
                            let tile1 = new Tile(<TColor>color1, number1, false);
                            let tile2 = new Tile(<TColor>color2, number2, false);
                            expect(() => {
                                new Pair(tile1, tile2);
                            }).to.throw();
                        }
                    }
                }
            }
        }
    });
});