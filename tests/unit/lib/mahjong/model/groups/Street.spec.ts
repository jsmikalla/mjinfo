import { expect } from 'chai';
import 'mocha';
import Tile, {TColor, TNumber} from "@/lib/mahjong/model/Tile";
import Street from "@/lib/mahjong/model/suites/Street";

describe('Chi', () => {

    let numberSimples: Array<TNumber> = ['2', '3', '4', '5', '6', '7', '8'];
    let numberTerminals: Array<TNumber> = ['1', '9'];
    let numberDragons: Array<TNumber> = ['red', 'white', 'green'];
    let numberWinds: Array<TNumber> = ['east', 'south', 'west', 'north'];
    let numberHonors: Array<TNumber> = numberWinds.concat(numberDragons);
    let numbers: Array<TNumber> = numberSimples.concat(numberTerminals).concat(numberDragons).concat(numberWinds);

    let colors = ['pin', 'man', 'bamboo', 'wind', 'dragon'];

    it('should only be constructed in straits with identical tiles', () => {
        expect(() => {
            new Street(
                new Tile('man', '1', false),
                new Tile('man', '2', false),
                new Tile('man', '3', false),
                false
            );
        }).to.not.throw();

        expect(() => {
            new Street(
                new Tile('bamboo', '5', true),
                new Tile('bamboo', '7', false),
                new Tile('bamboo', '6', false),
                false
            );
        }).to.not.throw();

        expect(() => {
            new Street(
                new Tile('pin', '4', false),
                new Tile('pin', '2', false),
                new Tile('pin', '3', false),
                false
            );
        }).to.not.throw();

        expect(() => {
            new Street(
                new Tile('pin', '1', false),
                new Tile('bamboo', '2', false),
                new Tile('pin', '3', false),
                false
            );
        }).to.throw('Street.ts/constructor: Invalid tile combination for a street');

        expect(() => {
            new Street(
                new Tile('dragon', 'red', false),
                new Tile('dragon', 'white', false),
                new Tile('dragon', 'green', false),
                false
            );
        }).to.throw('Street.ts/constructor: Invalid tile combination for a street');

        expect(() => {
            new Street(
                new Tile('wind', 'east', false),
                new Tile('wind', 'south', false),
                new Tile('wind', 'west', false),
                false
            );
        }).to.throw('Street.ts/constructor: Invalid tile combination for a street');

        expect(() => {
            new Street(
                new Tile('man', '9', false),
                new Tile('man', '1', false),
                new Tile('man', '2', false),
                false
            );
        }).to.throw('Street.ts/constructor: Invalid tile combination for a street');

        expect(() => {
            new Street(
                new Tile('dragon', 'green', false),
                new Tile('dragon', 'green', false),
                new Tile('dragon', 'green', false),
                false
            );
        }).to.throw('Street.ts/constructor: Invalid tile combination for a street');
    });
});