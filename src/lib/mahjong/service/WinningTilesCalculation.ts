import ReadyHand from "@/lib/mahjong/model/ReadyHand";
import Tile, {TNumber} from "@/lib/mahjong/model/Tile";
import Hand from "@/lib/mahjong/model/Hand";
import WinningState from "@/lib/mahjong/model/WinningState";

import {TTilesOfColor} from "@/lib/mahjong/service/TileLibrary";
import ScoreCalculation from "@/lib/mahjong/service/ScoreCalculation";

import {allValidColorsAndNumbers} from "@/lib/mahjong/constants/AllValidColorsAndNumbers";

export default class WinningTilesCalculation {

    /**
     * Returns true if with [readyHand] and [tile] under [winningState] a complete winning hand can be build
     * @param readyHand
     * @param tile
     * @param winningState
     */
    private static canBuildHandWith(
        readyHand: ReadyHand,
        tile: Tile,
        winningState: WinningState
    ): boolean {
        let hand = new Hand(
            readyHand.closedTiles,
            readyHand.closedQuads,
            readyHand.openTriplets,
            readyHand.openQuads,
            readyHand.openStreets,
            tile
        );

        // try getting a score for this combination, if yes it is a valid combination
        let score = ScoreCalculation.calculateScore(
            hand,
            winningState,
            [],
            [],
            null
        );

        return score !== null;
    }

    /**
     * Returns [tiles] ordered by color and number
     * @param tiles
     */
    private static generateTColorInfo(tiles: Array<Tile>) : Array<TTilesOfColor> {
        let tilesList: Array<TTilesOfColor> = [];

        for (let colorInfo of allValidColorsAndNumbers) {
            let tilesOfColor: TTilesOfColor = {
                colorName: colorInfo.color,
                numbers: []
            };

            for (let number of colorInfo.numbers) {
                let numberInfo: {number: TNumber, isRed: boolean, tiles: Array<Tile>} =
                    {number: number, isRed: false, tiles: []};

                for (let tile of tiles) {
                    if (tile.tileColor === colorInfo.color && tile.tileNumber === number) {
                        numberInfo.tiles.push(tile);
                    }
                }

                tilesOfColor.numbers.push(numberInfo);
            }

            tilesList.push(tilesOfColor);
        }

        return tilesList;
    }

    /**
     * Returns on which tiles a [readyHand] could win under [winningState],
     * considering [unusedTiles] are left to choose from
     * @param readyHand
     * @param winningState
     * @param unusedTiles
     */
    public static getAllWinningTiles(
        readyHand: ReadyHand,
        winningState: WinningState,
        unusedTiles: Array<TTilesOfColor>
    ): Array<TTilesOfColor> | null {
        let tiles: Array<Tile> = [];

        for (let color of unusedTiles) {
            for (let number of color.numbers) {
                if (number.tiles.length > 0) {
                    let tileToProbe = number.tiles[0];
                    if (WinningTilesCalculation.canBuildHandWith(readyHand, tileToProbe, winningState)) {
                        tiles.push(tileToProbe);
                    }

                }
            }
        }

        return WinningTilesCalculation.generateTColorInfo(tiles);
    }
}