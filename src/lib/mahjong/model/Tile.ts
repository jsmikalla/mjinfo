/*
 * THe "normal" colors of a tile
 */
export type TColorNonHonor = 'man' | 'pin' | 'bamboo';
/**
 * The "special" (honor) colors of a tile
 */
export type TColorHonor = 'wind' | 'dragon';
/**
 * All colors possible for a tile
 */
export type TColor = TColorNonHonor | TColorHonor;

/**
 * The numbers of a tile which are counted as "terminal"
 */
export type TNumberTerminal = '1' | '9';
/**
 * The numbers of a tile which are counted as "simples"
 */
export type TNumberSimple = '2' | '3' | '4' | '5' | '6' | '7' | '8';
/**
 * The numbers of dragon color (honor)
 */
export type TNumberDragonHonor = 'red' | 'white' | 'green';
/**
 * The numbers of wind color (honor)
 */
export type TNumberWindHonor = 'east' | 'south' | 'west' | 'north';
/**
 * All possible numbers of a tile
 */
export type TNumber = TNumberTerminal | TNumberSimple | TNumberDragonHonor | TNumberWindHonor;

export type TTileType = 'simple' | 'terminal' | 'honor';

export default class Tile {

    /**
     * Returns true if the number, color, and redAttribute together form a legal tile
     * @param tileColor
     * @param tileNumber
     * @param isRedFive
     */
    public static isValidCombinationForTile(tileColor: TColor, tileNumber: TNumber, isRedFive: boolean) {
        if (isRedFive && tileNumber !== '5') {
            return false;
        }

        switch (tileColor) {
            case 'man':
            case 'pin':
            case 'bamboo':
                return tileNumber === '1' || tileNumber === '2' || tileNumber === '3' ||
                    tileNumber === '4' || tileNumber === '5' || tileNumber === '6' ||
                    tileNumber === '7' || tileNumber === '8' || tileNumber === '9';

            case 'dragon':
                return tileNumber === 'red' || tileNumber === 'white' || tileNumber === 'green';

            case 'wind':
                return tileNumber === 'east' || tileNumber === 'south' ||
                    tileNumber === 'west' || tileNumber === 'north';
        }
    }

    /**
     * The current tiles color
     */
    public readonly tileColor: TColor;

    /**
     * The current tiles number
     */
    public readonly tileNumber: TNumber;

    /**
     * True if the tile is a red five tile
     */
    public readonly isRedFive: boolean;

    /**
     * The tileType (simple / terminal / honor)
     */
    get tileType(): TTileType {
        switch (this.tileNumber) {
            case '1':
            case '9':
                return 'terminal';

            case 'red':
            case 'white':
            case 'green':
            case 'east':
            case 'south':
            case 'west':
            case 'north':
                return 'honor';

            default:
                return 'simple';
        }
    }

    /**
     * Returns what the next number in a street needs to be for this tile
     * null if this has to be the final tile
     */
    get chiSuccessorNumber(): TNumber | null {
        switch (this.tileNumber) {
            case '1':
                return '2';
            case '2':
                return '3';
            case '3':
                return '4';
            case '4':
                return '5';
            case '5':
                return '6';
            case '6':
                return '7';
            case '7':
                return '8';
            case '8':
                return '9';
            default:
                return null;
        }
    }

    /**
     * Represents a play tile
     * @param tileColor
     * @param tileNumber
     * @param isRedFive
     */
    constructor(tileColor: TColor, tileNumber: TNumber, isRedFive: boolean) {
        if (isRedFive && tileNumber !== '5') {
            throw new RangeError('Tile.ts/constructor: Invalid number for red five');
        }

        if (!Tile.isValidCombinationForTile(tileColor, tileNumber, isRedFive)) {
            throw new RangeError('Tile.ts/constructor: Invalid color / number combination');
        }

        this.tileColor = tileColor;
        this.tileNumber = tileNumber;
        this.isRedFive = isRedFive;
    }
}