export type TMiniPointsType = 'FU_POINTS_OPEN'
    | 'FU_POINTS_COMPLETELY_CLOSED'
    | 'FU_POINTS_SEVEN_PAIRS'
    | 'FU_POINTS_TRIPLET'
    | 'FU_POINTS_QUAD'
    | 'FU_POINTS_SELF_DRAWN'
    | 'FU_POINTS_SINGLE_TILE_WAIT'
    | 'FU_POINTS_DRAGON_PAIR'
    | 'FU_POINTS_SEAT_WIND_PAIR'
    | 'FU_POINTS_ROUND_WIND_PAIR'
    | 'FU_POINTS_OPEN_PINFU';

export default interface MiniPoints {
    getScore(): number;
    getType(): TMiniPointsType;
}