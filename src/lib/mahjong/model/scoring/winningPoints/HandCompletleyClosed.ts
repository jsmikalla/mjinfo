import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class HandCompletleyClosed implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_MENSEN_TSUMO';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}