import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class ThreeQuadsOrTriplets implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_THREE_CLOSED_QUADS_OR_TRIPLETS';
    }

    public getScore(): number {
        return 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}