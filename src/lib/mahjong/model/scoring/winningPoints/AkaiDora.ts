import WinningPoints, {TWinningPointsType} from "../WinningPoints";

import Tile from "@/lib/mahjong/model/Tile";

export default class AkaiDora implements WinningPoints {

    private readonly tile: Tile;

    public getScore(): number {
        return 1;
    }

    public getType(): TWinningPointsType {
        return 'YAKU_DORA_AKAI';
    }

    public getTile(): Tile {
        return this.tile;
    }

    public isSufficientToWin(): boolean {
        return false;
    }

    constructor(tile: Tile) {
        if (!tile.isRedFive) {
            throw new RangeError('Akai dora requires a red five');
        }

        this.tile = tile;
    }
}