import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class RoundWindTriplet implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_ROUND_WIND_TRIPLET_OR_QUAD';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}