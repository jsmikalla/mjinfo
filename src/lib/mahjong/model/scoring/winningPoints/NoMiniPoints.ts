import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class NoMiniPoints implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_NO_MINI_POINTS';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}
