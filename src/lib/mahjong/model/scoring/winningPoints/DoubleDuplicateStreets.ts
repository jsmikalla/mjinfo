import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class DoubleDuplicateStreets implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_DOUBLE_TWINS';
    }

    public getScore(): number {
        return 3;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}