import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class FirstRoundAfterRichiiDeclaration implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_IPPATSU';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}