import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class LastTileSelfDrawn implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_HAITEI';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}