import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class DoubleRichii implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_DOUBLE_RICHII';
    }

    public getScore(): number {
        return 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}
