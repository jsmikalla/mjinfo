import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class LastTileOtherDrawn implements WinningPoints {

    public getType(): TWinningPointsType {
        return 'YAKU_HOUTEI';
    }

    public getScore(): number {
        return 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}