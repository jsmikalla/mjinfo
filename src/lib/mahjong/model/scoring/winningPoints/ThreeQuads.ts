import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class ThreeQuads implements WinningPoints {
    public getType(): TWinningPointsType {
        return 'YAKU_THREE_QUADS';
    }

    public getScore(): number {
        return 2;
    }

    public isSufficientToWin(): boolean {
        return true;
    }
}