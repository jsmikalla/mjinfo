import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class ParallelStreets implements WinningPoints {

    private isClosed: boolean;

    public getType(): TWinningPointsType {
        return 'YAKU_PARALLEL_STREETS';
    }

    public getScore(): number {
        return this.isClosed ? 2 : 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    constructor(isClosed: boolean) {
        this.isClosed = isClosed;
    }

}