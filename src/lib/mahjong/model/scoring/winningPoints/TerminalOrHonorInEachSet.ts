import WinningPoints, {TWinningPointsType} from "../WinningPoints";

export default class TerminalOrHonorInEachSet implements WinningPoints {

    private readonly isClosed: boolean;

    public getType(): TWinningPointsType {
        return 'YAKU_CHANTA';
    }

    public getScore(): number {
        return this.isClosed ? 2 : 1;
    }

    public isSufficientToWin(): boolean {
        return true;
    }

    constructor(isClosed: boolean) {
        this.isClosed = isClosed;
    }

}