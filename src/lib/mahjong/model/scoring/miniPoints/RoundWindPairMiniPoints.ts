import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class RoundWindPairMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_ROUND_WIND_PAIR';
    }

    public getScore(): number {
        return 2;
    }
}