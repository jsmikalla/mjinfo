import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class SelfDrawnMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_SELF_DRAWN';
    }

    public getScore(): number {
        return 2;
    }
}