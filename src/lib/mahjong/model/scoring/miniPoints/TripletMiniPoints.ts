import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class TripletMiniPoints implements MiniPoints {

    private readonly tripletOfTerminalsOrHonors: boolean;
    private readonly closedTripelt: boolean;

    public getType(): TMiniPointsType {
        return 'FU_POINTS_TRIPLET';
    }

    public getScore(): number {
        return 2 * (this.tripletOfTerminalsOrHonors ? 2 : 1) * (this.closedTripelt ? 2 : 1);
    }

    constructor(tripletOfTerminalsorHonors: boolean, closedTriplet: boolean) {
        this.tripletOfTerminalsOrHonors = tripletOfTerminalsorHonors;
        this.closedTripelt = closedTriplet;
    }
}