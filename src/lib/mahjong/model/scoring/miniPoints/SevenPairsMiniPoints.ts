import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class SevenPairsMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_SEVEN_PAIRS';
    }

    public getScore(): number {
        return 25;
    }
}