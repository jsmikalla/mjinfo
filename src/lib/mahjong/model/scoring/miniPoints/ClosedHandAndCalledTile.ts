import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class ClosedHandAndCalledTile implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_COMPLETELY_CLOSED';
    }

    public getScore(): number {
        return 30;
    }
}