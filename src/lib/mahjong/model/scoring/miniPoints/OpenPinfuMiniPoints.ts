import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class OpenPinfuMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_OPEN_PINFU';
    }

    public getScore(): number {
        return 2;
    }
}