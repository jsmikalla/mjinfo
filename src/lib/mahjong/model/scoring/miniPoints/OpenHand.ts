import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class OpenHand implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_OPEN';
    }

    public getScore(): number {
        return 20;
    }
}