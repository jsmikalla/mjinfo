import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class SingleTileWaitMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_SINGLE_TILE_WAIT';
    }

    public getScore(): number {
        return 2;
    }
}