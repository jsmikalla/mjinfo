import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class DragonPairMiniPoints implements MiniPoints {

    public getType(): TMiniPointsType {
        return 'FU_POINTS_DRAGON_PAIR';
    }

    public getScore(): number {
        return 2;
    }
}