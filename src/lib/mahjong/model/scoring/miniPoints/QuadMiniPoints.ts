import MiniPoints, {TMiniPointsType} from "@/lib/mahjong/model/scoring/MiniPoints";

export default class QuadMiniPoints implements MiniPoints {

    private readonly quadOfTerminalsOrHonors: boolean;
    private readonly closedQuad: boolean;

    public getType(): TMiniPointsType {
        return 'FU_POINTS_QUAD';
    }

    public getScore(): number {
        return 8 * (this.quadOfTerminalsOrHonors ? 2 : 1) * (this.closedQuad ? 2 : 1);
    }

    constructor(quadOfTerminalsOrHonors: boolean, closedQuad: boolean) {
        this.quadOfTerminalsOrHonors = quadOfTerminalsOrHonors;
        this.closedQuad = closedQuad;
    }
}