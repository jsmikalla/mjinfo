export type TWinningPointsType =
    'YAKU_DORA_AKAI'
    | 'YAKU_DORA'
    | 'YAKU_DORA_URA'
    | 'YAKU_SEVEN_PAIRS'
    | 'YAKU_RICHII'
    | 'YAKU_MENSEN_TSUMO'
    | 'YAKU_IPPATSU'
    | 'YAKU_HONITSU'
    | 'YAKU_CHINITSU'
    | 'YAKU_ITTSU'
    | 'YAKU_IPPEKU'
    | 'YAKU_TANYAO'
    | 'YAKU_CHANTA'
    | 'YAKU_JUNCHAN'
    | 'YAKU_HAITEI'
    | 'YAKU_HOUTEI'
    | 'YAKU_DEAD_WALL_DRAW'
    | 'YAKU_QUAD_ROBBED'
    | 'YAKU_DOUBLE_TWINS'
    | 'YAKU_PARALLEL_STREETS'
    | 'YAKU_ALL_TRIPLETS_OR_QUADS'
    | 'YAKU_THREE_QUADS'
    | 'YAKU_THREE_CLOSED_QUADS_OR_TRIPLETS'
    | 'YAKU_THREE_PARALLEL_TRIPLETS_OR_KANS'
    | 'YAKU_DRAGON_TRIPLET_OR_QUAD'
    | 'YAKU_SEAT_WIND_TRIPLET_OR_QUAD'
    | 'YAKU_ROUND_WIND_TRIPLET_OR_QUAD'
    | 'YAKU_LITTLE_THREE_DRAGONS'
    | 'YAKU_DOUBLE_RICHII'
    | 'YAKU_NO_MINI_POINTS';

export default interface WinningPoints {
    getScore(): number;
    getType(): TWinningPointsType;
    isSufficientToWin(): boolean;
}