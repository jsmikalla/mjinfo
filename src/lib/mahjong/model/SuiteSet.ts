import Quad from "@/lib/mahjong/model/suites/Quad";
import Triplet from "@/lib/mahjong/model/suites/Triplet";
import Pair from "@/lib/mahjong/model/suites/Pair";
import Street from "@/lib/mahjong/model/suites/Street";
import Tile from "@/lib/mahjong/model/Tile";

export type TSuiteSetType = 'HAND_CONFIGURATION_NORMAL' | 'HAND_CONFIGURATION_SEVEN_PAIRS';

/**
 * Represents a set of suites a hand is divided into
 */
export default interface SuiteSet {

    /**
     * The pair suites in this set
     */
    getPairs(): Array<Pair>;

    /**
     * The street suites in this set
     */
    getStreets(): Array<Street>;

    /**
     * The triplet suites in this set
     */
    getTriplets(): Array<Triplet>;

    /**
     * The quad suite in this set
     */
    getQuads(): Array<Quad>;

    /**
     * All tiles in all suites
     */
    getAllTiles(): Array<Tile>;

    /**
     * An id of a suite set
     */
    getType(): TSuiteSetType;

    /**
     * True if the suite is completely closed
     */
    isClosed(): boolean;
}