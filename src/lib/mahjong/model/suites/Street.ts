import Tile, {TNumber} from "@/lib/mahjong/model/Tile";
import Suite, {TSuiteType} from "@/lib/mahjong/model/Suite";

export default class Street implements Suite {

    /**
     * Returns true if firstTile, secondTile and thirdTile together form a valid street suite
     * @param firstTile
     * @param secondTile
     * @param thirdTile
     */
    public static isValidStreet(firstTile: Tile, secondTile: Tile, thirdTile: Tile): boolean {
        // all tiles need to be the same color
        if (
            firstTile.tileColor !== secondTile.tileColor ||
            firstTile.tileColor !== thirdTile.tileColor
        ) {
            return false;
        }

        // What needs to come as a tile after the first, second and third tile
        // null means there can be no sucessor to this tile and it has to be last
        let firstTileSuccessor = firstTile.chiSuccessorNumber;
        let secondTileSuccessor = secondTile.chiSuccessorNumber;
        let thirdTileSuccessor = thirdTile.chiSuccessorNumber;

        let numberOfNulls =
            (firstTileSuccessor === null ? 1 : 0) +
            (secondTileSuccessor === null ? 1 : 0) +
            (thirdTileSuccessor === null ? 1 : 0);
        if (numberOfNulls > 1) {
            // If more then one tile needs to be the last tile, this won't work
            return false;
        }

        // try all combinations
        return ( // 1 -> 2 -> 3
            firstTileSuccessor === secondTile.tileNumber
            && secondTileSuccessor === thirdTile.tileNumber
        ) || ( // 1 -> 3 -> 2
            firstTileSuccessor === thirdTile.tileNumber
            && thirdTileSuccessor === secondTile.tileNumber
        ) || ( // 2 -> 1 -> 3
            secondTileSuccessor === firstTile.tileNumber
            && firstTileSuccessor === thirdTile.tileNumber
        ) || ( // 2 -> 3 -> 1
            secondTileSuccessor === thirdTile.tileNumber
            && thirdTileSuccessor === firstTile.tileNumber
        ) || ( // 3 -> 1 -> 2
            thirdTileSuccessor === firstTile.tileNumber
            && firstTileSuccessor === secondTile.tileNumber
        ) || ( // 3 -> 2 -> 1
            thirdTileSuccessor === secondTile.tileNumber
            && secondTileSuccessor === firstTile.tileNumber
        );
    }

    public readonly lowestTile: Tile;
    public readonly midTile: Tile;
    public readonly upperTile: Tile;
    public readonly isOpen: boolean;

    /**
     * True if the suite counts as an open suite for scoring
     */
    public getIsOpen(): boolean {
        return this.isOpen;
    }

    /**
     * An id to distinguish suite type
     */
    public getSuiteType(): TSuiteType {
        return 'chi';
    }

    /**
     * The tiles forming the suite
     */
    public getTiles(): Array<Tile> {
        return [this.lowestTile, this.midTile, this.upperTile];
    }

    /**
     * Returns true if a tile in this suite is also in the other suite
     * @param otherSuite
     */
    public containsSameTileAs(otherSuite: Suite): boolean {
        for (let tile of otherSuite.getTiles()) {
            if (tile === this.lowestTile || tile === this.midTile || tile === this.upperTile) {
                return true;
            }
        }

        return false;
    }

    /**
     * Represents a street suite
     * @param firstTile
     * @param secondTile
     * @param thirdTile
     * @param isOpen
     */
    constructor(
        firstTile: Tile,
        secondTile: Tile,
        thirdTile: Tile,
        isOpen: boolean,
    ) {
        if (!Street.isValidStreet(firstTile, secondTile, thirdTile)) {
            throw new RangeError('Street.ts/constructor: Invalid tile combination for a street');
        }

        // What needs to come as a tile after the first, second and third tile
        // null means there can be no sucessor to this tile and it has to be last
        let firstTileSuccessor = firstTile.chiSuccessorNumber;
        let secondTileSuccessor = secondTile.chiSuccessorNumber;
        let thirdTileSuccessor = thirdTile.chiSuccessorNumber;

        // try to find the right order
        switch (true) {
            case firstTileSuccessor === secondTile.tileNumber
            && secondTileSuccessor === thirdTile.tileNumber:

                this.lowestTile = firstTile;
                this.midTile = secondTile;
                this.upperTile = thirdTile;
                break;

            case firstTileSuccessor === thirdTile.tileNumber
            && thirdTileSuccessor === secondTile.tileNumber:

                this.lowestTile = firstTile;
                this.midTile = thirdTile;
                this.upperTile = secondTile;
                break;

            case secondTileSuccessor === firstTile.tileNumber
            && firstTileSuccessor === thirdTile.tileNumber:

                this.lowestTile = secondTile;
                this.midTile = firstTile;
                this.upperTile = thirdTile;
                break;

            case secondTileSuccessor === thirdTile.tileNumber
            && thirdTileSuccessor === firstTile.tileNumber:

                this.lowestTile = secondTile;
                this.midTile = thirdTile;
                this.upperTile = firstTile;
                break;

            case thirdTileSuccessor === firstTile.tileNumber
            && firstTileSuccessor === secondTile.tileNumber:

                this.lowestTile = thirdTile;
                this.midTile = firstTile;
                this.upperTile = secondTile;
                break;

            case thirdTileSuccessor === secondTile.tileNumber
            && secondTileSuccessor === firstTile.tileNumber:

                this.lowestTile = thirdTile;
                this.midTile = secondTile;
                this.upperTile = firstTile;
                break;

            default:
                throw new RangeError('Street.ts/constructor: Switch trapped');
        }

        this.isOpen = isOpen;
    }
}